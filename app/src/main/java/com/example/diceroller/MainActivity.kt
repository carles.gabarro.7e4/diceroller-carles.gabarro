package com.example.diceroller

import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    lateinit var rollButton: Button
    lateinit var resetButton: Button
    lateinit var imageButton: ImageButton
    lateinit var imageButton2: ImageButton

    var array = intArrayOf(R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val mediaPlayer: MediaPlayer = MediaPlayer.create(this, R.raw.sound)

        val toast = Toast.makeText(this, "JACKPOT!", Toast.LENGTH_SHORT)
        rollButton = findViewById(R.id.roll_button)
        imageButton = findViewById(R.id.dice_1)
        imageButton2 = findViewById(R.id.dice_2)
        resetButton = findViewById(R.id.reset_button)

        rollButton.setOnClickListener {
            mediaPlayer.start()
            imageButton.setImageResource(array.random())
            imageButton2.setImageResource(array.random())
            toast.show()
        }

        resetButton.setOnClickListener{
            imageButton.setImageResource(R.drawable.empty_dice)
            imageButton2.setImageResource(R.drawable.empty_dice)
        }

        imageButton.setOnClickListener {
            mediaPlayer.start()
            imageButton.setImageResource(array.random())
        }
        imageButton2.setOnClickListener {
            mediaPlayer.start()
            imageButton2.setImageResource(array.random())
        }

    }
}